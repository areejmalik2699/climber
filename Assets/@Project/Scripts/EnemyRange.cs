using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : MonoBehaviour
{
    public Vector2 pos1;
    public Vector2 pos2;
    public Vector2 posDiff = new Vector2(7f, 0f);
    float speed = 0.5f;
    Vector2 previousPosition;
    void Start()
    {
        pos1 = transform.position;
        pos2 = pos1 + posDiff;
        previousPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 currentPos = Vector2.Lerp(pos1, pos2, Mathf.PingPong(Time.time * speed, 1.0f));

        Vector2 direction = currentPos - previousPosition;
        if (direction.x > 0) 
        {
            
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (direction.x < 0)
        {
            
            GetComponent<SpriteRenderer>().flipX = false;
        }
        previousPosition = currentPos;

        transform.position = currentPos;
    }
}
