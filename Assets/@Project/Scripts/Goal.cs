﻿using UnityEngine;

// Script to control goals
public class Goal : MonoBehaviour
{
    // SE to play when you reach the goal
    public AudioClip m_goalClip;

	// Did you score a goal?
	private bool m_isGoal;
	public GameObject winningPanel;
    // Function called when colliding with another object
    private void OnTriggerEnter2D( Collider2D other )
	{
		// まだゴールしておらず
		if ( !m_isGoal )
		{
			// 名前に「Player」が含まれるオブジェクトと当たったら
			if ( other.name.Contains( "Player" ) )
			{
				// シーンに存在する CameraShaker スクリプトを検索する
				var cameraShake = FindObjectOfType<CameraShaker>();

				// CameraShaker を使用してカメラを揺らす
				cameraShake.Shake();

				// 何回もゴールしないように、ゴールしたことを記憶しておく
				m_isGoal = true;

				// ゴールのアニメーターを取得する
				var animator = GetComponent<Animator>();

				// ゴールした時のアニメーションを再生する
				animator.Play( "Pressed" );

				// ゴールした時の SE を再生する
				var audioSource = FindObjectOfType<AudioSource>();
				audioSource.PlayOneShot( m_goalClip );
                winningPanel.SetActive(true);

            }
		}
	}
}