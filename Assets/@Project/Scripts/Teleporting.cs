using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class Teleporting : MonoBehaviour
{
    public Transform portalPosition;
    public GameObject particleA, particleB;
    //public GameObject Destroyer;
    //public float distance;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Contains("Player"))
        {
            StartCoroutine(Teleport(other.transform));
        }
    }

    IEnumerator Teleport(Transform obj)
    {
        // Activate particle A
        obj.gameObject.SetActive(false);
        particleA.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        obj.position = portalPosition.position;
        yield return new WaitForSeconds(0.1f);
        obj.gameObject.SetActive(true);
        particleB.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        particleA.SetActive(false);
        particleB.SetActive(false);
    }

}
