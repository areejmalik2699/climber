using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void LevelOne()
    {
        SceneManager.LoadScene("TestScene");
    }
    public void LevelTwo()
    {
        SceneManager.LoadScene("LevelTwo");
    }
    public void LevelThree()
    {
        SceneManager.LoadScene("LevelThree");
    }

    public void MenuScreen()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
