﻿using PC2D;
using UnityEngine;

// Script to control the enemy
public class Enemy : MonoBehaviour
{
    // Prefab for the hit animation
    public GameObject m_hitPrefab;

    // SE to play when hit
    public AudioClip m_hitClip;

    // Component to control enemy movement
    private PlatformerMotor2D m_motor;

    // Component to display enemy sprite
    private SpriteRenderer m_renderer;

    // Component to manage enemy collider
    private BoxCollider2D m_collider;

    // Function called when the scene starts
    private void Awake()
    {
        // Get the component to control enemy movement
        m_motor = GetComponent<PlatformerMotor2D>();

        // Initially move left
        m_motor.normalizedXMovement = -1;

        // Get the component to display enemy sprite
        m_renderer = GetComponent<SpriteRenderer>();

        // Initially face left
        m_renderer.flipX = false;

        // Get the component to manage enemy collider
        m_collider = GetComponent<BoxCollider2D>();
    }

    // Function called every frame
    private void Update()
    {
        // Get the current direction
        var dir = 0 < m_motor.normalizedXMovement
            ? Vector3.right
            : Vector3.left;

        // Check if there is a tile map in the current direction
        var offset = m_collider.size.y * 0.5f;
        var hit = Physics2D.Raycast
        (
            transform.position - new Vector3(0, offset, 0),
            dir,
            m_collider.size.x * 0.55f,
            Globals.ENV_MASK
        );

        // If there is a tile map in the current direction
        if (hit.collider != null)
        {
            // Reverse the movement direction
            m_motor.normalizedXMovement = -m_motor.normalizedXMovement;

            // Flip the sprite
            m_renderer.flipX = !m_renderer.flipX;
        }
    }

    // Function called when colliding with other objects
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.name.Contains("Bullet"))
        {
            Destroy(gameObject);
            Destroy(other.gameObject);

            var cameraShake = FindObjectOfType<CameraShaker>();
            cameraShake.Shake();
            Instantiate(m_hitPrefab, transform.position, Quaternion.identity);
            var audioSource = FindObjectOfType<AudioSource>();

            audioSource.PlayOneShot(m_hitClip);

        }
        else if (other.name.Contains("Player"))
        {
            // Get the component to control player movement
            var motor = other.GetComponent<PlatformerMotor2D>();

            // If the player is falling
            if (motor.IsFalling())
            {
                // Destroy the enemy
                Destroy(gameObject);

                // Make the player jump
                motor.ForceJump();

                // Find the CameraShaker script in the scene
                var cameraShake = FindObjectOfType<CameraShaker>();

                // Shake the camera using CameraShaker
                cameraShake.Shake();

                // Instantiate the hit animation object
                Instantiate(m_hitPrefab, transform.position, Quaternion.identity);

                // Play the hit SE
                var audioSource = FindObjectOfType<AudioSource>();
                audioSource.PlayOneShot(m_hitClip);

                // Prevent playing the jump SE when the player jumps
                var player = other.GetComponent<Player>();
                player.IsSkipJumpSe = true;
            }
            // If the player is not falling
            else
            {
                // Get the Player script from the player
                var player = other.GetComponent<Player>();

                // Call the function to handle player death
                player.Dead();
            }
        }
    }
}
