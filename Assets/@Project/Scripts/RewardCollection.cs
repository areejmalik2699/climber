using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Timeline.TimelinePlaybackControls;

public class RewardCollection : MonoBehaviour
{
    public float rotationSpeed = 50f;
    void Update()
    {
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Contains("Player"))
        {
            Shoot.instance.coinsCount++;
            Shoot.instance.coinText.text = "" + Shoot.instance.coinsCount;
            transform.gameObject.SetActive(false);
        }
    }
}
