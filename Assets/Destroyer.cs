using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public float upSpeed;
    public float initialWait;
    float time = 0;
    private void Update()
    {
        if (time < initialWait)
        {
            time += Time.deltaTime;
            return;

        }

        transform.position += new Vector3(0, 1*upSpeed*Time.deltaTime, 0);
    }
}
