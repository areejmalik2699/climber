using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{
    public GameObject applePrefab;
    public GameObject bananaPrefab;
    public float appleSpeed = 10f, bananaSpeed=15f;
    public float bulletLifetime = 3f;

    public int appleCount = 10;
    public int bananaCount = 5;
    public int coinsCount = 0;
    private enum Direction { None, Left, Right }
    private Direction shootingDirection = Direction.None;
    public static Shoot instance;

    public Text appleText, bananaText, coinText;

    private void Awake()
    {
        instance = this;
    }
     void Start()
    {
        bananaText.text = "" + bananaCount;
        appleText.text = "" + appleCount;
        coinText.text = "" + coinsCount;
    }

    private void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            shootingDirection = Direction.Left;
        }
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            shootingDirection = Direction.Right;
        }

        if (Input.GetKeyDown(KeyCode.O) && appleCount > 0)
        {
            ShootBullet(applePrefab, ref appleCount, ref appleSpeed);
            appleText.text = "" + appleCount;
        }
        else if (Input.GetKeyDown(KeyCode.I) && bananaCount > 0)
        {
            ShootBullet(bananaPrefab, ref bananaCount, ref bananaSpeed);
            bananaText.text = "" + bananaCount;
        }
    }

    private void ShootBullet(GameObject bulletPrefab, ref int bulletCount, ref float speed)
    {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();

        // Set bullet velocity based on shooting direction
        if (shootingDirection == Direction.Left)
        {
            rb.velocity = Vector2.left * speed;
        }
        else if (shootingDirection == Direction.Right)
        {
            rb.velocity = Vector2.right * speed;
        }

        // Destroy the bullet after its lifetime
        Destroy(bullet, bulletLifetime);

        // Reduce bullet count
        bulletCount--;
    }
}
